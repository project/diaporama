CHANGELOG for Diaporama for Drupal 6

Diaporama 6.x-2.0-BETA1
  Bug Fixed:
    o Display error under Internet Explorer is fixed
    o Correction of right errors on diaproama directories
  
  New Features:
  	o Keep ratio checkbox is now checked by default
  	o Images which keep ratio have a background color.
  	o Administrator can define images's background color
  	o Embed slideshow available
  	o New directory structure, new image generations

Diaporama 6.x-1.1
  Bug Fixed:
    o #333584, #282740: Keep javascript in front of Flash Slideshow
  
  New Features:
    o Add messages and tests for files rights.
    o Add <hr/> after images selection in diaporama form.
    o #332525: Diaporama can be linked to a gallery
    o #332525: Thanks to the cron, diaporama is updated when images are added to gallery.

Diaporama 6.x-1.0
  Bug Fixed:
    none
  New Features:
    Porting drupal6
       
----------------------------------------------------------------------
Diaporama 6.x-2.0-BETA1
  Anomalies corrigées:
    o Correction de l'erreur d'affichage sous Internet Explorer
    o Correction de l'erreur de droits du répertoire
  
  Nouvelles fonctionnalités:
  	o La case à cocher "Conserver le ratio" est désormais cochée par défaut.
  	o Les images qui conservent leur ratio ont désormais un fond de couleur
  	o L'administrateur peut définir le fond de couleur des images
  	o Il est possible de proposer un diaporama embarqué
  	o Nouvelle structure de répertoire et nouvelle génération des images

Diaporama 6.x-1.1
  Anomalies corrigées:
    o #333584, #282740: Le javascript passe devant le diaporama flash
  
  Nouvelles fonctionnalités:
    o Ajoute des messages et test les droits des fichiers
    o Ajoute une balise <hr /> après l'affichage des miniatures dans le formulaire
    o #332525: Le diaporama peut etre lie a une galerie photo
    o #332525: Via le cron, le diaporama est mis à jour si une image est ajoutée à une galerie qui lui est lie.

Diaporama 6.x-1.0
  Anomalies corrigées:
    none
  Nouvelles fonctionnalités:
    Porting drupal6
